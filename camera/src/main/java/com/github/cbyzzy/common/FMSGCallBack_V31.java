package com.github.cbyzzy.common;

import com.github.cbyzzy.hik.HCNetSDK;
import com.github.cbyzzy.service.HikCameraService;
import com.sun.jna.Pointer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class FMSGCallBack_V31 implements HCNetSDK.FMSGCallBack_V31 {


    //lCommand 上传消息类型  pAlarmer 报警设备信息  pAlarmInfo  报警信息   dwBufLen 报警信息缓存大小   pUser  用户数据
    @Override
    public boolean invoke(int lCommand, HCNetSDK.NET_DVR_ALARMER pAlarmer, Pointer pAlarmInfo, int dwBufLen, Pointer pUser) {
        //报警类型
        String sAlarmType = "lCommand=0x" + Integer.toHexString(lCommand);
        //报警时间
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        //序列号
        String sSerialNumber = byteToString(pAlarmer.sSerialNumber);
        //用户id
        String lUserID = new String(String.valueOf(pAlarmer.lUserID));
        //ip地址
        String sDeviceIP = byteToString(pAlarmer.sDeviceIP);

        /**
         * lCommand是传的报警类型
         * *********************回调函数类型 begin************************
         *public static final int COMM_ALARM = 0x1100;    //8000报警信息主动上传
         *public static final int COMM_TRADEINFO = 0x1500;  //ATMDVR主动上传交易信息
         *public static final int COMM_ALARM_V30 = 0x4000;//9000报警信息主动上传
         *public static final int COMM_ALARM_V40 = 0x4007;
         *public static final int COMM_ALARM_RULE = 0x1102;//行为分析信息上传
         *public static final int COMM_ALARM_PDC = 0x1103;//客流量统计报警上传
         *public static final int COMM_UPLOAD_PLATE_RESULT = 0x2800;//交通抓拍结果上传
         *public static final int COMM_ITS_PLATE_RESULT = 0x3050;//交通抓拍的终端图片上传
         *public static final int COMM_IPCCFG = 0x4001;//9000设备IPC接入配置改变报警信息主动上传
         *public static final int COMM_ITS_PARK_VEHICLE = 0x3056;//停车场数据上传
         *public static final int COMM_VEHICLE_CONTROL_ALARM = 0x3059;
         *public static final int COMM_ALARM_TFS = 0x1113; //交通取证报警信息
         *public static final int COMM_ALARM_TPS_V41 = 0x1114; //交通事件报警信息扩展
         *public static final int COMM_ALARM_AID_V41 = 0x1115; //交通事件报警信息扩展
         *public static final int COMM_UPLOAD_FACESNAP_RESULT = 0x1112;  //人脸识别结果上传
         *public static final int COMM_SNAP_MATCH_ALARM = 0x2902;
         *public static final int COMM_ALARM_ACS = 0x5002; //门禁主机报警信息
         *public static final int COMM_ID_INFO_ALARM = 0x5200; //门禁身份证刷卡信息
         *public static final int COMM_VCA_ALARM = 0x4993; //智能检测通用报警
         *public static final int COMM_ISAPI_ALARM = 0x6009;//ISAPI协议报警信息
         *public static final int COMM_ALARM_TPS_STATISTICS = 0x3082; //TPS统计过车数据上传
         *public static final int COMM_ALARMHOST_CID_ALARM = 0x1127;  //报告报警上传
         *public static final int COMM_UPLOAD_VIDEO_INTERCOM_EVENT = 0x1132;  //可视对讲事件记录上传
         *public static final int COMM_ALARM_VIDEO_INTERCOM = 0x1133;  //可视对讲报警上传
         */

        switch (lCommand) {
            //移动侦测
            case HCNetSDK.COMM_ALARM_V30:
                log.info("COMM_ALARM_V30 = 0x4000 ---> 9000报警信息主动上传");
                //报警类型
                log.info("sAlarmType：======{}", sAlarmType);
                //设备序列号
                log.info("sSerialNumber：======{}", sSerialNumber);
                //用户id
                log.info("lUserID：======{}", lUserID);
                //设备ip
                log.info("sDeviceIP：======{}", sDeviceIP);
                //当前时间
                log.info("date：======{}", date);
                break;
            case HCNetSDK.COMM_ALARM:
                log.info("COMM_ALARM = 0x1100 ----> 8000报警信息主动上传");

            default:
                log.info("其他报警信息=========={}");
                break;
        }

        return false;
    }

    /**
     * 处理返回的信息 字节转字符串
     *
     * @param bytes
     * @return
     */
    private static String byteToString(byte[] bytes) {
        String[] strings = new String(bytes).split("\0", 2);
        StringBuilder sb = new StringBuilder();
        if (strings != null && strings.length > 0) {
            for (int i = 0; i < strings.length - 1; i++) {
                sb.append(strings[i]);
            }
        }
        return sb.toString();
    }



}
